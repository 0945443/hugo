---
title: Design Challenge
date: 11-09-2017
---

Vandaag hadden we onze eerste stand-up, een korte presentatie over waar je nu bent met je project. Ik had daarbij het woord gedaan. We hebben er een goede feedback op gekregen, dat het goed was om met een van de organisatoren te praten van de introductieweken voor bouwkunde. Na deze stand-up gehoord te hebben van iedereen gingen we aan de slag aan de paper prototype. We hebben elkaars prototype's vergeleken en aan de hand daarvan hebben we een nieuwe gemaakt. 

We zijn ook al wat gaan kijken naar de lay-out van de game zoals welke kleuren we zouden gaan gebruiken. De paper-prototype willen we allemaal op A4 papiertjes maken zodat het beter te zien is tijdens onze presentatie van volgende week. 

Bij de workshop vandaag hebben we een blog aan moeten maken via gitlab. Dat was een beetje een gedoe bij iedereen doordat de website een beetje raar deed, maar uiteindelijk deed mijn blog het gelukkig. 

> Written with [StackEdit](https://stackedit.io/).