---
title: Design Challenge
date: 06-09-2017
---

Vandaag zijn we met ons team naar het hogeschool gebouw van bouwkunde geweest. Daar hebben we wat leerlingen geïnterviewd, en een van de organisatoren van de introductieweken voor bouwkunde. Met haar hebben we een fijn gesprek gehad en hebben we erg veel informatie ontvangen die we mee kunnen nemen in onze game. 

Terug op school hebben we nog even verder gebraindstormd over het spel, en heb ik een naam bedacht voor de game: TeamBuilding. Omdat de game ervoor moet zorgen dat de studenten elkaar leren kennen en omdat ze bouwkunde doen natuurlijk. Eind van de middag kregen we een workshop over hoe we onze blogs moeten bijhouden. 

Ook had ik vandaag mijn eerste studiecoaching. Met ons team gingen we regels bespreken die we in onze groep graag willen hebben. Ook moesten we ons eigen SWOT maken, wat erg interessant was want zo leer je je teamgenoten nog beter kennen. Een drukke maar leuke dag!

> Written with [StackEdit](https://stackedit.io/).