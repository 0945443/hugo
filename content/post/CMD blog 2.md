---
title: Hoorcollege Design Theorie
date: 05-09-2017
---

De eerste hoorcollege Design Theorie. Erg interessant en de leerkracht is erg grappig waardoor je je aandacht er goed bijhoud. We hebben de stappen en regels van een ontwerpproces geleerd en gezien, en wat design nou precies is. Deze stappen zijn ook zeer handig om weer mee te nemen in ons eigen project. 
 
Vanuit onze aantekeningen moeten we een samenvatting maken, om die weer mee te nemen voor ons tentamen. 

![enter image description here](https://lh3.googleusercontent.com/Hbfc9RoMtW_OjMqu8rD8L_Mw_iJ4yoPySBcWPqDokc5ukkaueX4PUj3P8pBo9Nehq_I0bV-9Jzw=s0 "IMG_4578 3.JPG")
![enter image description here](https://lh3.googleusercontent.com/-C39ap8TFxt0/WbZm76_JqnI/AAAAAAAAAHY/XJvdWrnos7gwNrE6qXst_llt7_XVWzNHACLcBGAs/s0/FullSizeRender+2.jpg "FullSizeRender 2.jpg")







> Written with [StackEdit](https://stackedit.io/).

