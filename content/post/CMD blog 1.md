---
title:Design Challenge
date:04-09-2017
---

Vandaag was de eerste dag dat we met ons groepje aan het werk mochten. We hebben de opdracht gekregen om een spel te maken voor een introductie week van Hogeschool Rotterdam in 2018. Na de korte uitleg zijn we begonnen met een onderzoek naar de benodigdheden voor een game (app). We waren het er over eens om als doelgroep de eerstejaars studenten van bouwkunde te gebruiken.

Voor vandaag hadden we de planning gemaakt dat iedereen wat onderzoek zou doen naar de afleiding bouwkunde, en hebben we besproken om deze week nog naar de locatie toe te gaan om wat studenten te interviewen.  Ook hebben we afgesproken dat iedereen een eigen paper prototype gaat maken om ze later te vergelijken met mekaar. 






> Written with [StackEdit](https://stackedit.io/).