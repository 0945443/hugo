---
title: Design Theorie
date: 07-09-2017
---

Vandaag moesten we de stof van de hoorcollege toepassen door middel van een opdracht te maken. Eerst ging je individueel te werk aan een visueel schema van iets wat je ooit hebt georganiseerd. Daarna presteerden we dat voor onze groep en kozen we de beste uit. We werkte het proces opnieuw uit en presenteerde het voor de klas. 

We hebben een goede feedback gekregen, alleen is het handiger volgende keer de presentatie op meerdere papieren te laten zien.

Het doel van deze opdracht was om zo goed mogelijk een proces te ondergaan om iets te ontwerpen/ op te lossen. Hierbij hebben we gebruik gemaakt van de design theorie: hoe je vanuit een huidige situatie tot een gewenste situatie komt. 

Na deze les zijn we nog even aan onze poster met 'teamregels' verder gaan werken. We waren het snel over de regels eens en gaan ze zeker toepassen.

![enter image description here](https://lh3.googleusercontent.com/--O023BfPGRY/WbZmRHy4HBI/AAAAAAAAAHE/UdKt0AbpqiAq8w6MbtWNdeNhHD5Pd28ngCLcBGAs/s0/IMG_4574.JPG "IMG_4574.JPG")


> Written with [StackEdit](https://stackedit.io/).